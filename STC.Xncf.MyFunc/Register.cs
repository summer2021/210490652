﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.XncfBase;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using STC.Xncf.MyFunc.Functions;

namespace STC.Xncf.MyFunc
{
    [XncfRegister]
    public partial class Register : XncfRegisterBase, IXncfRegister
    {
        #region IXncfRegister 接口

        public override string Name => "STC.Xncf.MyFunc";

        public override string Uid => "878308BA-1289-41F4-984D-A6A13C252B1A";//必须确保全局唯一，生成后必须固定，已自动生成，也可自行修改

        public override string Version => "1.2";//必须填写版本号

        public override string MenuName => "函数";

        public override string Icon => "fa fa-star";

        public override string Description => "函数";

        public override IList<Type> Functions => new Type[] { typeof(MyFunction) };



        public override async Task InstallOrUpdateAsync(IServiceProvider serviceProvider, InstallOrUpdate installOrUpdate)
        {
        }

        public override async Task UninstallAsync(IServiceProvider serviceProvider, Func<Task> unsinstallFunc)
        {
            await unsinstallFunc().ConfigureAwait(false);
        }
        #endregion
    }
}
