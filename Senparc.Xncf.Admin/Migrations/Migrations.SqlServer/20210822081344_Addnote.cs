﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Senparc.Xncf.Admin.Migrations.Migrations.SqlServer
{
    public partial class Addnote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Senparc_Admin_User",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Note",
                table: "Senparc_Admin_User");
        }
    }
}
