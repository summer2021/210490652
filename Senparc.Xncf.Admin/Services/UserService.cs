﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using Senparc.Xncf.Admin.Models.DatabaseModel.Dto;
using Senparc.Xncf.Admin.Models.DatabaseModel;
using System;
using System.Threading.Tasks;

namespace Senparc.Xncf.Admin.Services
{
    public class UserService : ServiceBase<User>
    {
        public UserService(IRepositoryBase<User> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        internal Task CreateOrUpdateAsync(UserDto userDto)
        {
            throw new NotImplementedException();
        }
    }
}
